/**
 * defines all our java script modules
 */
modules = {

    'app-js' {
    }

    'app' {
        dependsOn 'app-js'  // application-specific angular-related scripts ...
    }
}

package moa

class TagLink {

    static constraints = {
    }

    Tag tag

    SupportsMetaData owner

    static mapping = {
        version false
    }
}

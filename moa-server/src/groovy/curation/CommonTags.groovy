package curation

/**
 * bunch of common tag labels for easier sharing
 * User: wohlgemuth
 * Date: 10/1/14
 * Time: 1:49 PM
 */
public interface CommonTags {
    public static final String RELATIVE_SPECTRA = "relative spectra"

    public static final String LCMS_SPECTRA = "LCMS"

    public static final String GCMS_SPECTRA = "GCMS"

    public static final String VIRTUAL_COMPOUND = "virtually derivatized compound"

    public static final String INVALID_DERIVATIZATION = "invalid derivatization"

    public static final String INCHI_KEY_DOESNT_MATCH_MOLECULE = "InChI Key does not match molecule!"

    public static final String DIRTY_SPECTRA = "noisy spectra"

    public static final String ANNOTATED_SPECTRA = "annotated"

    public static final String SUSPECT_VALUE = "suspect value"

    public static final String DUPLICATED_SPECTRA = "duplicated"



}
package util

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 9/25/14
 * Time: 4:18 PM
 */
class StaticProperties {

    /**
     * unless otherwise specified, we never return more hits by a query
     */
    static int MAX_QUERY_RESULTS = 25000
}
